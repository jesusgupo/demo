package es.sample.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Clase de configuracion de Swagger.
 * 
 * @author jgpovedano
 * @date 02/03/2020
 * @version 1.0
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	/**
	 * Constructor por defecto de la clase.
	 */
	public SwaggerConfig() {
		super();
	}
	
	/**
	 * Metodo de inicializacion del Docket Swagger2.
	 * 
	 * @return docket.
	 */
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          .paths(PathSelectors.any())                          
          .build();                                           
    }
	
}  // Final SwaggerConfig.