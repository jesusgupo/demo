package es.sample.demo.config;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jndi.JndiObjectFactoryBean;

/**
 * Clase que inicializa el datasource de la aplicacion.
 * 
 * @author jgpovedano
 * @date 29/02/2020
 * @version 1.0.
 * @see https://docs.spring.io/spring-boot/docs/current/reference/html/appendix-application-properties.html
 */
@Configuration
//Se habilita el reconocimiento de las implementaciones de Spring-Data/JPA. Descarte por subpaquete
//@EnableJpaRepositories(basePackages = "es.sample.demo.persistence.dao")	
//Se habilita el reconocimiento de las JPA-Entities. Descartado al estar en subpaquete.
@EntityScan(basePackages = "es.sample.demo.persistence.entity")			
public class JPAConfig {

	// Atributos de la clase.
	@Value(value = "${spring.datasource.url:null}")
	private String url;
	
	@Value(value = "${spring.datasource.username:null}")	
	private String user;
	
	@Value(value = "${spring.datasource.driver-class-name:null}")	
	private String driverClass;
	
	@Value(value = "${spring.datasource.password:null}")	
	private String password;
	
	@Value(value = "${spring.datasource.jndi-name:null}")
	private String jndi;
	
	/**
	 * Constructor por defecto de la clase.
	 */
	public JPAConfig() {
		super();
	}
	
	/**
	 * Metodo que permite crear el datasource para el entorno de desarrollo.
	 * 
	 * @return datasource para desarrollo local.
	 */
    @SuppressWarnings("rawtypes")
	@Bean
	@Profile(value = "dev")
    public DataSource getDataSource() {
        final DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(driverClass);
        dataSourceBuilder.url(url);
        dataSourceBuilder.username(user);
        dataSourceBuilder.password(password);
        return dataSourceBuilder.build();
    }	
	
	/**
	 * Metodo que permite crear el datasource para el entorno de explotacion.
	 * 
	 * @return datasource para explotacion.
	 */    
    @Bean
	@Profile(value = "pro")
    public DataSource jndiDataSource() throws IllegalArgumentException, NamingException {
        final JndiObjectFactoryBean jndiBean = new JndiObjectFactoryBean();    
        jndiBean.setJndiName(jndi);
        jndiBean.setProxyInterface(DataSource.class);
        jndiBean.setLookupOnStartup(true);
        
        return (DataSource) jndiBean.getObject();
    }	
	
}  // Final JPAConfig.
