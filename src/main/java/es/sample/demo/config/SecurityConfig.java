package es.sample.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Clase que permite inicializar la seguridad en el sistema.
 * 
 * @author jgpovedano
 * @date 03/03/2020
 * @version 1.0
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	/**
	 * Constructor por defecto de la clase.
	 */
	public SecurityConfig() {
		super();
	}
	
	/**
	 * {@inheritDoc}
	 */
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http.
         csrf().disable().	// Peticiones POST
         authorizeRequests().anyRequest().authenticated().	// Todas las peticiones con seguridad.
         and().
         httpBasic();
    }
  
	/**
	 * {@inheritDoc}
	 */
    @Override
    public void configure(WebSecurity web) throws Exception {
    	// Se permite Swagger.
        web.ignoring().antMatchers("/v2/api-docs",
                                   "/configuration/ui",
                                   "/swagger-resources/**",
                                   "/configuration/security",
                                   "/swagger-ui.html",
                                   "/webjars/**");
    }
    
    /**
     * Metodo que permite configurar la autenticacion en memoria.
     * 
     * @param auth builder del authentication manager.
     * @throws Exception excepcion producida durante la ejecucion del metodo.
     */
    @Autowired
    public void configureGlobal(final AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
            .withUser("sdos")
            .password("{noop}sdos")
            .roles("USER");
    }
    
}  // Final SecurityConfig