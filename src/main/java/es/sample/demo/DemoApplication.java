package es.sample.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Clase de inicializacion del sistema sobre el servidor Tomcat
 * embebido.
 * 
 * @author jgpovedano
 * @date 29/02/2020
 * @version 1.0
 */
@SpringBootApplication		// Se habilita @EnableAutoConfiguration y @ComponentScan
public class DemoApplication {

	/**
	 * Constructor por defecto de la clase.
	 */
	public DemoApplication() {
		super();
	}
	
	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}  // Final DemoApplication.
