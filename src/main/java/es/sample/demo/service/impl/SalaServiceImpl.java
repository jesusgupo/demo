package es.sample.demo.service.impl;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import es.sample.demo.domain.SalaTO;
import es.sample.demo.mapper.DemoMapper;
import es.sample.demo.persistence.dao.RecursoRepository;
import es.sample.demo.persistence.dao.ReservaRepository;
import es.sample.demo.persistence.dao.SalaRepository;
import es.sample.demo.persistence.entity.RecursoEntity;
import es.sample.demo.persistence.entity.ReservaEntity;
import es.sample.demo.persistence.entity.SalaEntity;
import es.sample.demo.service.SalaService;

/**
 * Implementacion del servicio que gestiona las entidades de tipo sala.
 * 
 * @author jgpovedano
 * @date 29/02/2020
 * @version 1.0
 */
@Service
@Transactional(propagation = Propagation.SUPPORTS)
public class SalaServiceImpl implements SalaService {

	// Constantes de la clase.
	private static final int ROWS_PER_PAGE = 10;
	
	// Atributos de la clase.
	@Autowired
	private SalaRepository salaRepository;
	
	@Autowired
	private RecursoRepository recursoRepository;
	
	@Autowired
	private ReservaRepository reservaRepository;	
	
	/**
	 * Constructor por defecto de la clase.
	 */
	public SalaServiceImpl() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<SalaTO> obtenerSalas(final int numPagina) {
		List<SalaTO> salaList = null;
		
		// Se define el componente de paginacion para la consulta y se realiza la busqueda sobre el repositorio..
        final Page<SalaEntity> pagedResult = salaRepository.findAll(PageRequest.of(numPagina, ROWS_PER_PAGE));
         
        // En caso de no obtener resultados se devolvera la lista vacia. En caso positivo, se emplea el mapper.
        if (pagedResult.hasContent()) {
        	salaList = DemoMapper.getMapperFacade().mapAsList(pagedResult.getContent(), SalaTO.class);
        } else {
            salaList = new ArrayList<SalaTO>();
        }		
		return salaList;
	}

	/**
	 * {@inheritDoc}
	 */	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)	
	public boolean crearSala(final SalaTO sala) {
		boolean completed = false;
		
		if (sala != null) {
			// Se realiza el mapeo desde la entidad de dominio hasta la entidad de persistencia.
			final SalaEntity entity = DemoMapper.getMapperFacade().map(sala, SalaEntity.class);
			
			// Se realiza la persistencia.
			for (RecursoEntity rec : entity.getRecursos()) {
				rec.setSala(entity);
			}
			final SalaEntity savedEntity = salaRepository.save(entity);
			if (savedEntity.getIdSala() != null) {
				completed = true;
			}
		}
		return completed;
	}

	/**
	 * {@inheritDoc}
	 */		
	@Override
	@Transactional(propagation = Propagation.REQUIRED)	
	public boolean modificarSala(final SalaTO sala) {
		boolean completed = false;
		
		if (sala != null) {
			final Optional<SalaEntity> optionalEntity = salaRepository.findByCodigoSala(sala.getCodSala());
			if (optionalEntity.isPresent()) {
				final SalaEntity salaEntity = optionalEntity.get();

				/*
				 * Solo se modifican atributos simples y recursos.
				 * 
				 * Lo normal es validar la presencia en el parametro y evitar establecer un valor
				 * a nulo. Podemos marcarlo requerido en el controlador, etc., pero lo hacemos asi por 
				 * agilizar.
				 */
				salaEntity.setAforo(sala.getAforo());
				salaEntity.setNombreSala(sala.getNombre());
				
				// Damos por hecho que la lista de recursos del parametro de entrada viene informado. Lo normal
				// seria validarlo. 
				salaEntity.getRecursos().stream().
					forEach(rec -> recursoRepository.delete(rec));
				
				final List<RecursoEntity> nuevosRecursos = DemoMapper.getMapperFacade().mapAsList(sala.getRecursos(), RecursoEntity.class);
				nuevosRecursos.stream().forEach(nr -> nr.setSala(salaEntity));
				salaEntity.setRecursos(nuevosRecursos);
				
				// Se realiza la operacion.
				salaRepository.save(salaEntity);
				completed = true;
			}
		}
		return completed;
	}

	/**
	 * {@inheritDoc}
	 */	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public boolean eliminarSala(final SalaTO sala) {
		boolean completed = false;
		
		if (sala != null && sala.getCodSala() != null && !"".equals(sala.getCodSala())) {
			
			/*
			 * Solo borraremos una sala si no tiene reservas en ningun momento. Es una decision de validacion
			 * que no tiene sentido, pero simplemente se incluye para realizar una operativa adicional de check.
			 */
			final List<ReservaEntity> reservas = reservaRepository.getReservasBySala(sala.getCodSala());
			boolean isRemovable = reservas.isEmpty();
			
			if (isRemovable) {
				/*
				 * Se propaga la eliminacion de la sala hasta los recursos asignados.
				 * 
				 * En una version completa se podria plantear que los recursos quedaran 
				 * vinculados a "nadie", con la FK nula a la espera de ser reasignados (ubicados) 
				 * en una nueva sala pero esta demo no tiene gestión de recursos.
				 */
				salaRepository.deleteByCodigoSala(sala.getCodSala());
				completed = true;	
			}
		}
		return completed;
	}	
	
}  // Final SalaServiceImpl