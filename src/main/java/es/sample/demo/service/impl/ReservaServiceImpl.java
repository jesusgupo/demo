package es.sample.demo.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import es.sample.demo.domain.ReservaTO;
import es.sample.demo.domain.SolicitudReservaTO;
import es.sample.demo.mapper.DemoMapper;
import es.sample.demo.persistence.dao.ReservaRepository;
import es.sample.demo.persistence.dao.SalaRepository;
import es.sample.demo.persistence.entity.RecursoEntity;
import es.sample.demo.persistence.entity.ReservaEntity;
import es.sample.demo.persistence.entity.SalaEntity;
import es.sample.demo.service.ReservaService;

/**
 * Implementacion del servicio que gestiona las entidades de tipo reservas.
 * 
 * @author jgpovedano
 * @date 01/03/2020
 * @version 1.0
 */
@Service
public class ReservaServiceImpl implements ReservaService {

	// Atributos de la clase.
	@Autowired
	private ReservaRepository reservaRepository;
	
	@Autowired
	private SalaRepository salaRepository;	
	
	/**
	 * Constructor por defecto de la clase.
	 */
	public ReservaServiceImpl() {
		super();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public ReservaTO solicitarReserva(final SolicitudReservaTO solicitud) {
		ReservaTO result = null;
		final List<String> recursosSolicitados = new ArrayList<>();
		
		// Se itera sobre los recursos recibidos como parametro
		solicitud.getRecursos().stream().forEach(recurso -> {
			recursosSolicitados.add(recurso.getCodRecurso());
		});
		
		
		// Se obtienen las fechas de inicio y de fin de las reuniones.
		// El parametro lo dispone en tres campos aunque en BBDD se han modelado dos.
		final LocalDateTime fechaInicio = LocalDateTime.of(solicitud.getFecha(), solicitud.getHoraInicio());
		final LocalDateTime fechaFin = LocalDateTime.of(solicitud.getFecha(), solicitud.getHoraFinal());
		
		// Se obtienen los codigos de los recursos solicitados.
		
		/*
		 * Se obtienen las salas libres. En realidad, con una consulta optimizada, la base
		 * de datos con indices y todas las optimizaciones se podria hacer de forma inmediata,
		 * pero lo haremos por codigo dadas las circunstancias:
		 */
		final List<SalaEntity> salasLibres = salaRepository.findSalasLibresByFechas(fechaInicio, fechaFin);
	
		
		// Lo mejor seria filtrar en la consulta para obtener las salas con aforo adecuado, pero lo dejo asi para mostrar un Predicate.
		final List<SalaEntity> salasLibresAforo = salasLibres.stream().
					filter(buildPredicateAforo(solicitud.getNumAsistentes())).
					filter(sala -> checkRecursos(sala.getRecursos(), recursosSolicitados)).
					collect(Collectors.toList());
		
		
		if (salasLibresAforo != null && !salasLibresAforo.isEmpty()) {
			// Se crea la entidad de dominio de retorno.
			result = new ReservaTO();
			result.setFecha(solicitud.getFecha());
			result.setHoraFinal(solicitud.getHoraFinal());
			result.setHoraInicio(solicitud.getHoraInicio());
			result.setCodSala(salasLibresAforo.get(0).getCodigoSala());
			result.setNombreSala(salasLibresAforo.get(0).getNombreSala());
			
			// Se crea la entidad de persistencia.
			final ReservaEntity reservaEntity = DemoMapper.getMapperFacade().map(result,  ReservaEntity.class);
			reservaEntity.setSala(salasLibresAforo.get(0));
			reservaEntity.setFechaFin(fechaFin);
			reservaEntity.setFechaInicio(fechaInicio);
			reservaRepository.save(reservaEntity);
		}
		
		return result;
	}
	
		
	/**
	 * Metodo que permite obtener el predicado de comparacion de aforo con el numero
	 * de asistentes.
	 * 
	 * @param numAsistentes numero de asistentes.
	 * @return predicado sobre la entidad sala.
	 */
	private Predicate<SalaEntity> buildPredicateAforo(final int numAsistentes) {
		return sala -> sala.getAforo() >= numAsistentes;
	}
	
	/**
	 * Metodo que permite generar la condicion de cumplimiento con los recursos
	 * solicitados en la solicitud de la cita.
	 *  
	 * @param recursosSala recursos de los que dispone la sala.
	 * @param recursosSolicitados recursos solicitados.
	 * @return
	 */
	private boolean checkRecursos(final List<RecursoEntity> recursosSala, final List<String> recursosSolicitados) {
		final List<String> codigosSala = new ArrayList<String>();
		
		recursosSala.stream().forEach(recurso -> {
			codigosSala.add(recurso.getCodigoRecurso());
		});
		
		return codigosSala.containsAll(recursosSolicitados);
	}

	
	
}  // Final ReservaServiceImpl.