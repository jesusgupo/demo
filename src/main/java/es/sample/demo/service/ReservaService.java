package es.sample.demo.service;

import es.sample.demo.domain.ReservaTO;
import es.sample.demo.domain.SolicitudReservaTO;

/**
 * Interface que define el contrato de negocio para la gestion de las reservas.
 * 
 * @author jgpovedano
 * @date 03/01/2020
 * @version 1.0
 */
public interface ReservaService {

	/**
	 * Metodo que permite solicitar y generar una reserva de sala.
	 * 
	 * En caso de no poder efectuarse se retorna un objeto a nulo.
	 * 
	 * @param solicitud solicitud de la reserva.
	 * @return reserva realizada.
	 */
	ReservaTO solicitarReserva(SolicitudReservaTO solicitud);
	
}  // Final ReservaService.