package es.sample.demo.service;

import java.util.List;

import es.sample.demo.domain.SalaTO;

/**
 * Interface que define el contrato de negocio para la gestion de las salas.
 * 
 * @author jgpovedano
 * @date 29/02/2020
 * @version 1.0
 */
public interface SalaService {

	/**
	 * Metodo que permite realizar la busqueda de las salas existentes indicando
	 * unicamente el numero de pagina.
	 * 
	 * @param numPagina numero de pagina de la busqueda.
	 * @return listado de salas obtenidas o lista vacia en caso de no hallar resultados.
	 */
	List<SalaTO> obtenerSalas(int numPagina);
	
	boolean crearSala(SalaTO sala);
	
	boolean modificarSala(SalaTO sala);	
	
	boolean eliminarSala(SalaTO sala);
	
	
	
}  // Final SalaService.
