package es.sample.demo.domain;

import java.io.Serializable;

/**
 * Entidad de dominio que representa a la entidad de negocio del recurso.
 * 
 * @author jgpovedano
 * @date 29/02/2020
 * @version 1.0
 */
public final class RecursoTO implements Serializable {

	// Identificador de Serial Version para el Serialization Runtime.
	private static final long serialVersionUID = 5213429206460087499L;

	// Atributos de la clase.
	private String codRecurso;
	private String nombreRecurso;	
	
	public RecursoTO() {
		super();
	}

	/**
	 * Metodo que permite obtener la propiedad codRecurso.
	 * @return propiedad codRecurso.
	 */
	public String getCodRecurso() {
		return codRecurso;
	}

	/**
	 * Metodo que permite establecer la propiedad codRecurso.
	 *
	 * @param codRecurso propiedad codRecurso.
	 */
	public void setCodRecurso(String codRecurso) {
		this.codRecurso = codRecurso;
	}

	/**
	 * Metodo que permite obtener la propiedad nombreRecurso.
	 * @return propiedad nombreRecurso.
	 */
	public String getNombreRecurso() {
		return nombreRecurso;
	}

	/**
	 * Metodo que permite establecer la propiedad nombreRecurso.
	 *
	 * @param nombreRecurso propiedad nombreRecurso.
	 */
	public void setNombreRecurso(String nombreRecurso) {
		this.nombreRecurso = nombreRecurso;
	}


}  // Final RecursoTO.