package es.sample.demo.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Entidad de dominio que representa a la entidad de negocio de la sala.
 * 
 * @author jgpovedano
 * @date 29/02/2020
 * @version 1.0
 */
public final class SalaTO implements Serializable {

	// Identificador de Serial Version para el Serialization Runtime.
	private static final long serialVersionUID = 6305515117766931402L;
	
	// Atributos de la clase.
	private String codSala;
	private String nombre;
	private int aforo;
	private List<RecursoTO> recursos;
	
	/**
	 * Constructor por defecto de la clase.
	 */
	public SalaTO() {
		super();
	}

	/**
	 * Metodo que permite obtener la propiedad codSala.
	 * @return propiedad codSala.
	 */
	public String getCodSala() {
		return codSala;
	}

	/**
	 * Metodo que permite establecer la propiedad codSala.
	 *
	 * @param codSala propiedad codSala.
	 */
	public void setCodSala(String codSala) {
		this.codSala = codSala;
	}

	/**
	 * Metodo que permite obtener la propiedad nombre.
	 * @return propiedad nombre.
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * Metodo que permite establecer la propiedad nombre.
	 *
	 * @param nombre propiedad nombre.
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	/**
	 * Metodo que permite obtener la propiedad aforo.
	 * @return propiedad aforo.
	 */
	public int getAforo() {
		return aforo;
	}

	/**
	 * Metodo que permite establecer la propiedad aforo.
	 *
	 * @param aforo propiedad aforo.
	 */
	public void setAforo(int aforo) {
		this.aforo = aforo;
	}

	/**
	 * Metodo que permite obtener la propiedad recursos.
	 * @return propiedad recursos.
	 */
	public List<RecursoTO> getRecursos() {
		if (recursos == null) {
			recursos = new ArrayList<RecursoTO>();
		}
		return recursos;
	}

	/**
	 * Metodo que permite establecer la propiedad recursos.
	 *
	 * @param recursos propiedad recursos.
	 */
	public void setRecursos(List<RecursoTO> recursos) {
		this.recursos = recursos;
	}
	
}  // Final SalaTO.
