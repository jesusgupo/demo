package es.sample.demo.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

public class ReservaTO implements Serializable {

	// Identificador de Serial Version para el Serialization Runtime.
	private static final long serialVersionUID = -2718174440952080901L;

	// Atributos de la clase.
	private String codReserva;
	private String codSala;
	private String nombreSala;
	private LocalDate fecha;
	private LocalTime horaInicio;
	private LocalTime horaFinal;
	
	/**
	 * Constructor por defecto de la clase.
	 */
	public ReservaTO() {
		super();
	}

	/**
	 * Metodo que permite obtener la propiedad codSala.
	 * @return propiedad codSala.
	 */
	public String getCodSala() {
		return codSala;
	}

	/**
	 * Metodo que permite establecer la propiedad codSala.
	 *
	 * @param codSala propiedad codSala.
	 */
	public void setCodSala(String codSala) {
		this.codSala = codSala;
	}

	/**
	 * Metodo que permite obtener la propiedad fecha.
	 * @return propiedad fecha.
	 */
	public LocalDate getFecha() {
		return fecha;
	}

	/**
	 * Metodo que permite establecer la propiedad fecha.
	 *
	 * @param fecha propiedad fecha.
	 */
	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	/**
	 * Metodo que permite obtener la propiedad horaInicio.
	 * @return propiedad horaInicio.
	 */
	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	/**
	 * Metodo que permite establecer la propiedad horaInicio.
	 *
	 * @param horaInicio propiedad horaInicio.
	 */
	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	/**
	 * Metodo que permite obtener la propiedad horaFinal.
	 * @return propiedad horaFinal.
	 */
	public LocalTime getHoraFinal() {
		return horaFinal;
	}

	/**
	 * Metodo que permite establecer la propiedad horaFinal.
	 *
	 * @param horaFinal propiedad horaFinal.
	 */
	public void setHoraFinal(LocalTime horaFinal) {
		this.horaFinal = horaFinal;
	}

	/**
	 * Metodo que permite obtener la propiedad codReserva.
	 * @return propiedad codReserva.
	 */
	public String getCodReserva() {
		return codReserva;
	}

	/**
	 * Metodo que permite establecer la propiedad codReserva.
	 *
	 * @param codReserva propiedad codReserva.
	 */
	public void setCodReserva(String codReserva) {
		this.codReserva = codReserva;
	}

	/**
	 * Metodo que permite obtener la propiedad nombreSala.
	 * @return propiedad nombreSala.
	 */
	public String getNombreSala() {
		return nombreSala;
	}

	/**
	 * Metodo que permite establecer la propiedad nombreSala.
	 *
	 * @param nombreSala propiedad nombreSala.
	 */
	public void setNombreSala(String nombreSala) {
		this.nombreSala = nombreSala;
	}

	
}  // Final ReservaTO.
