package es.sample.demo.domain;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Entidad de dominio que representa a la entidad de negocio de la solicitud
 * de reserva de una sala.
 * 
 * @author jgpovedano
 * @date 01/03/2020
 * @version 1.0
 */
public final class SolicitudReservaTO implements Serializable {

	// Identificador de Serial Version para el Serialization Runtime.
	private static final long serialVersionUID = -2785432342823880718L;
	
	// Atributos de la clase.
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate fecha;
	
	@JsonFormat(pattern = "HH:mm:ss")
	private LocalTime horaInicio;
	
	@JsonFormat(pattern = "HH:mm:ss")
	private LocalTime horaFinal;
	private int numAsistentes;
	private List<RecursoTO> recursos;
	
	/**
	 * Constructor por defecto de la clase.
	 */
	public SolicitudReservaTO() {
		super();
	}

	/**
	 * Metodo que permite obtener la propiedad fecha.
	 * @return propiedad fecha.
	 */
	public LocalDate getFecha() {
		return fecha;
	}

	/**
	 * Metodo que permite establecer la propiedad fecha.
	 *
	 * @param fecha propiedad fecha.
	 */
	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	/**
	 * Metodo que permite obtener la propiedad horaInicio.
	 * @return propiedad horaInicio.
	 */
	public LocalTime getHoraInicio() {
		return horaInicio;
	}

	/**
	 * Metodo que permite establecer la propiedad horaInicio.
	 *
	 * @param horaInicio propiedad horaInicio.
	 */
	public void setHoraInicio(LocalTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	/**
	 * Metodo que permite obtener la propiedad horaFinal.
	 * @return propiedad horaFinal.
	 */
	public LocalTime getHoraFinal() {
		return horaFinal;
	}

	/**
	 * Metodo que permite establecer la propiedad horaFinal.
	 *
	 * @param horaFinal propiedad horaFinal.
	 */
	public void setHoraFinal(LocalTime horaFinal) {
		this.horaFinal = horaFinal;
	}

	/**
	 * Metodo que permite obtener la propiedad numAsistentes.
	 * @return propiedad numAsistentes.
	 */
	public int getNumAsistentes() {
		return numAsistentes;
	}

	/**
	 * Metodo que permite establecer la propiedad numAsistentes.
	 *
	 * @param numAsistentes propiedad numAsistentes.
	 */
	public void setNumAsistentes(int numAsistentes) {
		this.numAsistentes = numAsistentes;
	}

	/**
	 * Metodo que permite obtener la propiedad recursos.
	 * @return propiedad recursos.
	 */
	public List<RecursoTO> getRecursos() {
		return recursos;
	}

	/**
	 * Metodo que permite establecer la propiedad recursos.
	 *
	 * @param recursos propiedad recursos.
	 */
	public void setRecursos(List<RecursoTO> recursos) {
		this.recursos = recursos;
	}
	
	
}  // Final SolicitudReservaTO.