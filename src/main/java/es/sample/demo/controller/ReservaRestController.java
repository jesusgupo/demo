package es.sample.demo.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import es.sample.demo.controller.message.ApiDemo;
import es.sample.demo.domain.ReservaTO;
import es.sample.demo.domain.SolicitudReservaTO;
import es.sample.demo.service.ReservaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

/**
 * Clase que implementa el controlador de gestion de reservas.
 * 
 * @author jgpovedano
 * @date 03/03/2020
 * @version 1.0
 */
@Slf4j
@RestController
@RequestMapping("/reservas")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST})
@Api(tags = "Reservas", value = "Api de gestion de reservas")
public class ReservaRestController {

	// Atributos de la clase.
	@Autowired
	private ReservaService reservaService;	
	
	/**
	 * Constructor por defecto de la clase.
	 */
	public ReservaRestController() {
		super();
	}
	
	/**
	 * Metodo que permite obtener el listado de salas del sismtema.
	 * 
	 * @param numPagina numero de pagina.
	 * @return listado de salas.
	 */
	@ApiOperation(tags = "Reservas",
				  value = "Operacion de solicitud de reserva", 
				  notes = "Operacion para la reserva de una sala acorde a los requerimientos de entrada, etc...",
				  response = ApiDemo.class)
	@ApiResponses({ 
		@ApiResponse(code = HttpServletResponse.SC_OK, message = "Mensaje de operacion completada. Se delega en el tipo de dato la inclusión de errores."),
		@ApiResponse(code = HttpServletResponse.SC_INTERNAL_SERVER_ERROR, message = "INTERNAL SERVER ERROR"),
	@ApiResponse(code = HttpServletResponse.SC_UNAUTHORIZED, message = "UNAUTHORIZED")
	})	
    @PostMapping(path= "/solicitar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ApiDemo> obtenerSalas(@ApiParam(value = "Solicitud de Reserva") @RequestBody final SolicitudReservaTO solicitud) {
        final ReservaTO reserva = reservaService.solicitarReserva(solicitud);
        ApiDemo response = null;
        
        if (reserva != null) {
        	response = new ApiDemo("Reserva realizada con codigo de sala " + reserva.getCodSala() + " - " + reserva.getNombreSala(), null);
        } else {
        	response = new ApiDemo("Fue imposible realizar la reserva", null);
        }
        
        return new ResponseEntity(response, new HttpHeaders(), HttpStatus.OK); 
    }		
	
}  // Final ReservaRestController.
