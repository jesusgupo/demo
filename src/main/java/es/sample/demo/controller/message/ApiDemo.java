package es.sample.demo.controller.message;

import java.io.Serializable;

/**
 * Clase que representa al mensaje de respuesta en cada operacion REST.
 * 
 * @author jgpovedano
 * @date 01/03/2020
 * @version 1.0
 */
public final class ApiDemo implements Serializable {
	 
	// Identificador para el Serialization Runtime.
	private static final long serialVersionUID = -7935140854389942337L;
	
	// Atributos de la clase.
    private String message;
    private String error;
 
    /**
     * Constructor por defecto de la clase.
     */
    public ApiDemo() {
    	super();
    }
    
    /**
     * Constructor que recibe un mensaje y un error.
     * 
     * @param message mensaje resultado.
     * @param errors listado de errores.
     */    
    public ApiDemo(String message, String error) {
    	super();
    	
    	this.message = message;
    	this.error = error;
    }

	/**
	 * Metodo que permite obtener la propiedad message.
	 * @return propiedad message.
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * Metodo que permite establecer la propiedad message.
	 *
	 * @param message propiedad message.
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Metodo que permite obtener la propiedad error.
	 * @return propiedad error.
	 */
	public String getError() {
		return error;
	}

	/**
	 * Metodo que permite establecer la propiedad error.
	 *
	 * @param error propiedad error.
	 */
	public void setError(String error) {
		this.error = error;
	}
    
    
}  // Final ApiDemo.