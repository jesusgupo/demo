package es.sample.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import es.sample.demo.domain.SalaTO;
import es.sample.demo.service.SalaService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/salas")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST})
@Api(tags = "Salas", value = "Api para la gestion de salas")
public class SalaRestController {

	// Atributos de la clase.
	@Autowired
	private SalaService salaService;
	
	/**
	 * Constructor por defecto de la clase.
	 */
	public SalaRestController() {
		super();
	}
	
	/**
	 * Metodo que permite obtener el listado de salas del sismtema.
	 * 
	 * @param numPagina numero de pagina.
	 * @return listado de salas.
	 */
    @GetMapping(path= "/obtener", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SalaTO>> obtenerSalas(@RequestParam(defaultValue = "0") int numPagina) {
        final List<SalaTO> salaList = salaService.obtenerSalas(numPagina);
 
        return new ResponseEntity<List<SalaTO>>(salaList, new HttpHeaders(), HttpStatus.OK); 
    }	
	
	/**
	 * Metodo que permite obtener el listado de salas del sismtema.
	 * 
	 * @param numPagina numero de pagina.
	 * @return listado de salas.
	 */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	@PutMapping(path= "/crear", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SalaTO>> crearSala(@RequestBody SalaTO sala) {
    	ResponseEntity result = null;
        final boolean completed = salaService.crearSala(sala);
 
        if (completed) {
        	// En caso de eliminarse la sala simplemente retornamos un HTTP 200 OK.
        	result = ResponseEntity.ok().build();
        } else {
        	// Si no se encuentra la entidad a eliminar, se indica ACCEPTED.
        	result = new ResponseEntity(null, new HttpHeaders(), HttpStatus.ACCEPTED); 
        }
        return result;
    }	
    
	/**
	 * Metodo que permite obtener el listado de salas del sismtema.
	 * 
	 * @param numPagina numero de pagina.
	 * @return listado de salas.
	 */
    @SuppressWarnings("rawtypes")
	@PostMapping(path= "/modificar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SalaTO>> modificarSala(@RequestBody SalaTO sala) {
    	ResponseEntity result = null;
    	final boolean completed = salaService.modificarSala(sala);
 
        if (completed) {
        	// En caso de eliminarse la sala simplemente retornamos un HTTP 200 OK.
        	result = ResponseEntity.ok().build();
        } else {
        	// Si no se encuentra la entidad a eliminar, se indica ACCEPTED.
        	result = new ResponseEntity(null, new HttpHeaders(), HttpStatus.ACCEPTED); 
        }
        return result;
    }	
    
	/**
	 * Metodo que permite obtener el listado de salas del sismtema.
	 * 
	 * @param numPagina numero de pagina.
	 * @return listado de salas.
	 */
    @SuppressWarnings("rawtypes")
	@DeleteMapping(path= "/eliminar", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity eliminarSala(@RequestBody SalaTO sala) {
    	ResponseEntity result = null;
    	final boolean completed = salaService.eliminarSala(sala);
 
        if (completed) {
        	// En caso de eliminarse la sala simplemente retornamos un HTTP 200 OK.
        	result = ResponseEntity.ok().build();
        } else {
        	// Si no se encuentra la entidad a eliminar, se indica ACCEPTED.
        	result = new ResponseEntity(null, new HttpHeaders(), HttpStatus.ACCEPTED); 
        }
        return result;
    }	    
    
    
}  // Final SalaRestController.
