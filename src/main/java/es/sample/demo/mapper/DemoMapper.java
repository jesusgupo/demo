package es.sample.demo.mapper;

import es.sample.demo.domain.RecursoTO;
import es.sample.demo.domain.SalaTO;
import es.sample.demo.persistence.entity.RecursoEntity;
import es.sample.demo.persistence.entity.SalaEntity;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;

/**
 * Clase que permite realizar el mapeo entre entidades de persistencia y entidades
 * de dominio.
 * 
 * @author jgpovedano
 * @date 29/02/2020
 * @version 1.0
 */
public class DemoMapper {
	
	// Factoria de mapeo.
	private static final MapperFactory mapperFactory;
	
	static {
		mapperFactory = new DefaultMapperFactory.Builder().build();

		// Definicion de los mapeos en el sistema
		mapperFactory.classMap(SalaEntity.class, SalaTO.class).
			field("codigoSala", "codSala").
			field("nombreSala", "nombre").
			field("aforo", "aforo").
			field("recursos", "recursos").
			byDefault().register();		
		
		mapperFactory.classMap(RecursoEntity.class, RecursoTO.class).
			field("codigoRecurso", "codRecurso").
			byDefault().register();
	}

	/**
	 * Constructor por defecto de la clase.
	 */
	private DemoMapper() {
		// Constructor privado para evitar la instanciacion.
	}

	public static MapperFactory getMapperfactory() {
		return mapperFactory;
	}

	public static MapperFacade getMapperFacade() {
		return mapperFactory.getMapperFacade();
	}	
	
	
}  // Final DemoMapper
