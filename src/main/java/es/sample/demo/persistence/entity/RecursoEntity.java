package es.sample.demo.persistence.entity;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RECURSO")
@Access(AccessType.FIELD)
public class RecursoEntity {

	// Atributos de la clase.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_RECURSO")    
    private Long idRecurso;
     
    @Column(name = "CODIGO_RECURSO")
    private String codigoRecurso;
     
    @Column(name = "NOMBRE_RECURSO")
    private String nombreRecurso;
	
    @JoinColumn(name = "ID_SALA", referencedColumnName = "ID_SALA", nullable = true)
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    private SalaEntity sala;
    
	/**
	 * Constructor por defecto de la clase.
	 */
	public RecursoEntity() {
		super();
	}

	/**
	 * Metodo que permite obtener la propiedad idRecurso.
	 * @return propiedad idRecurso.
	 */
	public Long getIdRecurso() {
		return idRecurso;
	}

	/**
	 * Metodo que permite establecer la propiedad idRecurso.
	 *
	 * @param idRecurso propiedad idRecurso.
	 */
	public void setIdRecurso(Long idRecurso) {
		this.idRecurso = idRecurso;
	}

	/**
	 * Metodo que permite obtener la propiedad codigoRecurso.
	 * @return propiedad codigoRecurso.
	 */
	public String getCodigoRecurso() {
		return codigoRecurso;
	}

	/**
	 * Metodo que permite establecer la propiedad codigoRecurso.
	 *
	 * @param codigoRecurso propiedad codigoRecurso.
	 */
	public void setCodigoRecurso(String codigoRecurso) {
		this.codigoRecurso = codigoRecurso;
	}

	/**
	 * Metodo que permite obtener la propiedad nombreRecurso.
	 * @return propiedad nombreRecurso.
	 */
	public String getNombreRecurso() {
		return nombreRecurso;
	}

	/**
	 * Metodo que permite establecer la propiedad nombreRecurso.
	 *
	 * @param nombreRecurso propiedad nombreRecurso.
	 */
	public void setNombreRecurso(String nombreRecurso) {
		this.nombreRecurso = nombreRecurso;
	}

	/**
	 * Metodo que permite obtener la propiedad sala.
	 * @return propiedad sala.
	 */
	public SalaEntity getSala() {
		return sala;
	}

	/**
	 * Metodo que permite establecer la propiedad sala.
	 *
	 * @param sala propiedad sala.
	 */
	public void setSala(SalaEntity sala) {
		this.sala = sala;
	}
	
	
}  // Final RecursoEntity.
