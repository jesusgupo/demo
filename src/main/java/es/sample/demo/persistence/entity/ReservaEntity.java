package es.sample.demo.persistence.entity;

import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "RESERVA")
@Access(AccessType.FIELD)
public class ReservaEntity {

	// Atributos de la clase.
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_RESERVA")    
    private Long idReserva;
         
    @Column(name = "FECHA_INICIO")
    private LocalDateTime fechaInicio;

    @Column(name = "FECHA_FIN")
    private LocalDateTime fechaFin;
    
    @ManyToOne
    @JoinColumn(name = "ID_SALA", referencedColumnName = "ID_SALA", nullable = false)
    private SalaEntity sala;
    

    /**
     * Constructor por defecto de la clase.
     */
    public ReservaEntity() {
		super();
	}

	/**
	 * Metodo que permite obtener la propiedad idReserva.
	 * @return propiedad idReserva.
	 */
	public Long getIdReserva() {
		return idReserva;
	}

	/**
	 * Metodo que permite establecer la propiedad idReserva.
	 *
	 * @param idReserva propiedad idReserva.
	 */
	public void setIdReserva(Long idReserva) {
		this.idReserva = idReserva;
	}

	/**
	 * Metodo que permite obtener la propiedad sala.
	 * @return propiedad sala.
	 */
	public SalaEntity getSala() {
		return sala;
	}

	/**
	 * Metodo que permite establecer la propiedad sala.
	 *
	 * @param sala propiedad sala.
	 */
	public void setSala(SalaEntity sala) {
		this.sala = sala;
	}

	/**
	 * Metodo que permite obtener la propiedad fechaInicio.
	 * @return propiedad fechaInicio.
	 */
	public LocalDateTime getFechaInicio() {
		return fechaInicio;
	}

	/**
	 * Metodo que permite establecer la propiedad fechaInicio.
	 *
	 * @param fechaInicio propiedad fechaInicio.
	 */
	public void setFechaInicio(LocalDateTime fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	/**
	 * Metodo que permite obtener la propiedad fechaFin.
	 * @return propiedad fechaFin.
	 */
	public LocalDateTime getFechaFin() {
		return fechaFin;
	}

	/**
	 * Metodo que permite establecer la propiedad fechaFin.
	 *
	 * @param fechaFin propiedad fechaFin.
	 */
	public void setFechaFin(LocalDateTime fechaFin) {
		this.fechaFin = fechaFin;
	}
	
}  // Final ReservaEntity.