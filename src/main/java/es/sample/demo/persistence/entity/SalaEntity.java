package es.sample.demo.persistence.entity;

import java.util.List;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "SALA")
@Access(AccessType.FIELD)
public class SalaEntity {
 
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID_SALA")    
    private Long idSala;
     
    @Column(name = "CODIGO_SALA")
    private String codigoSala;
     
    @Column(name = "NOMBRE_SALA")
    private String nombreSala;
     
    @Column(name = "AFORO")
    private int aforo;
    
    // A la vista de la implementacion realizada, se establece FetchType EAGER para evitar el N + 1 Select.
    @OneToMany(mappedBy = "sala", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<RecursoEntity> recursos;

    @OneToMany(mappedBy = "sala", fetch = FetchType.LAZY)
    private List<ReservaEntity> reservas;
    
    
    /**
     * Constructor por defecto de la clase.
     */
    public SalaEntity() {
    	super();
    }

	/**
	 * Metodo que permite obtener la propiedad idSala.
	 * @return propiedad idSala.
	 */
	public Long getIdSala() {
		return idSala;
	}

	/**
	 * Metodo que permite establecer la propiedad idSala.
	 *
	 * @param idSala propiedad idSala.
	 */
	public void setIdSala(Long idSala) {
		this.idSala = idSala;
	}

	/**
	 * Metodo que permite obtener la propiedad codigoSala.
	 * @return propiedad codigoSala.
	 */
	public String getCodigoSala() {
		return codigoSala;
	}

	/**
	 * Metodo que permite establecer la propiedad codigoSala.
	 *
	 * @param codigoSala propiedad codigoSala.
	 */
	public void setCodigoSala(String codigoSala) {
		this.codigoSala = codigoSala;
	}

	/**
	 * Metodo que permite obtener la propiedad nombreSala.
	 * @return propiedad nombreSala.
	 */
	public String getNombreSala() {
		return nombreSala;
	}

	/**
	 * Metodo que permite establecer la propiedad nombreSala.
	 *
	 * @param nombreSala propiedad nombreSala.
	 */
	public void setNombreSala(String nombreSala) {
		this.nombreSala = nombreSala;
	}

	/**
	 * Metodo que permite obtener la propiedad aforo.
	 * @return propiedad aforo.
	 */
	public int getAforo() {
		return aforo;
	}

	/**
	 * Metodo que permite establecer la propiedad aforo.
	 *
	 * @param aforo propiedad aforo.
	 */
	public void setAforo(int aforo) {
		this.aforo = aforo;
	}

	/**
	 * Metodo que permite obtener la propiedad recursos.
	 * @return propiedad recursos.
	 */
	public List<RecursoEntity> getRecursos() {
		return recursos;
	}

	/**
	 * Metodo que permite establecer la propiedad recursos.
	 *
	 * @param recursos propiedad recursos.
	 */
	public void setRecursos(List<RecursoEntity> recursos) {
		this.recursos = recursos;
	}

	/**
	 * Metodo que permite obtener la propiedad reservas.
	 * @return propiedad reservas.
	 */
	public List<ReservaEntity> getReservas() {
		return reservas;
	}

	/**
	 * Metodo que permite establecer la propiedad reservas.
	 *
	 * @param reservas propiedad reservas.
	 */
	public void setReservas(List<ReservaEntity> reservas) {
		this.reservas = reservas;
	}
    
}  // Final SalaEntity;