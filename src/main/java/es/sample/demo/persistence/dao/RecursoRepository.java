package es.sample.demo.persistence.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.sample.demo.persistence.entity.RecursoEntity;

/**
 * Interface para la creacion de la implementacion por parte de Spring del soporte 
 * para la gestion de entidades de tipo Reserva.
 * 
 * @author jgpovedano
 * @date 01/03/2020
 * @version 1.0
 */
//Se anota como Repository por estereotiparlo. No es necesario.
@Repository
public interface RecursoRepository extends JpaRepository<RecursoEntity, Long> {

	@Query("Select rec from RecursoEntity rec where rec.sala.codigoSala = :codigoSala")
	List<RecursoEntity> getRecursosFromSala(@Param("codigoSala") String codigoSala);
	
    @Query("UPDATE RecursoEntity r set r.sala = null where r.sala.codigoSala = :codigoSala")
    @Modifying
	void liberarRecursos(@Param("codigoSala") String codigoSala);
	
}  // Final RecursoRepository.