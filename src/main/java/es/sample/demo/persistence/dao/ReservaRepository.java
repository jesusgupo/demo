package es.sample.demo.persistence.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import es.sample.demo.persistence.entity.ReservaEntity;

/**
 * Interface para la creacion de la implementacion por parte de Spring del soporte 
 * para la gestion de entidades de tipo Reserva.
 * 
 * @author jgpovedano
 * @date 01/03/2020
 * @version 1.0
 */
//Se anota como Repository por estereotiparlo. No es necesario.
@Repository
public interface ReservaRepository extends JpaRepository<ReservaEntity, Long> {

	/**
	 * Metodo que permite obtener las reservas de la sala.
	 * 
	 * @param codSala codigo de sala.
	 * @return resultado de la consulta.
	 */
	@Query(value = "select r from ReservaEntity r where r.sala.codigoSala = :codigoSala")
	List<ReservaEntity> getReservasBySala(@Param("codigoSala") String codSala);
	
}  // Final ReservaRepository.