package es.sample.demo.persistence.dao;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import es.sample.demo.persistence.entity.SalaEntity;

/**
 * Interface para la creacion de la implementacion por parte de Spring del soporte 
 * para la gestion de entidades de tipo Sala.
 * 
 * @author jgpovedano
 * @date 29/02/2020
 * @version 1.0
 */
// Se anota como Repository por estereotiparlo. No es necesario.
@Repository
public interface SalaRepository extends PagingAndSortingRepository<SalaEntity, Long> {

	/**
	 * Metodo que permite obtener una sala por su codigo.
	 * 
	 * @param codigoSala codigo de la sala.
	 * @return entidad de persistencia cuyo codigo coincide con el pasado como parametro.
	 */
	Optional<SalaEntity> findByCodigoSala(String codigoSala);
	
	/**
	 * Metodo que permite eliminar una sala por su codigo.
	 * 
	 * @param codigoSala codigo de la sala.
	 * @return numero de entidades eliminadas por el codigo.
	 */
    @Modifying
    List<SalaEntity> deleteByCodigoSala(String codigoSala);
    
    
    @Query(value = "SELECT * FROM SALA WHERE ID_SALA NOT IN "
    				+ " (SELECT ID_SALA FROM RESERVA where (?1 BETWEEN FECHA_INICIO AND FECHA_FIN or ?2 BETWEEN FECHA_INICIO AND FECHA_FIN or (?1 >= FECHA_INICIO AND ?2 <= FECHA_FIN)))", 
    	  nativeQuery = true)
    List<SalaEntity> findSalasLibresByFechas(LocalDateTime fechaDesde, LocalDateTime fechaHasta);

    
}  // Final SalaRepository.