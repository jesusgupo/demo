

--- INSERTS
insert into SALA(NOMBRE_SALA, CODIGO_SALA, AFORO) VALUES ('SALA PRIMERA PLANTA', 'SAL01', 20);
insert into SALA(NOMBRE_SALA, CODIGO_SALA, AFORO) VALUES ('SALA SEGUNDA PLANTA', 'SAL02', 30);
insert into SALA(NOMBRE_SALA, CODIGO_SALA, AFORO) VALUES ('SALA PLANTA BAJA', 'SAL03', 15);
insert into SALA(NOMBRE_SALA, CODIGO_SALA, AFORO) VALUES ('SALA PLANTA BAJA', 'SAL04', 15);
insert into SALA(NOMBRE_SALA, CODIGO_SALA, AFORO) VALUES ('SALA PLANTA BAJA', 'SAL05', 12);
insert into SALA(NOMBRE_SALA, CODIGO_SALA, AFORO) VALUES ('SALA PLANTA BAJA', 'SAL06', 10);


-- Recursos por salas
insert into RECURSO(NOMBRE_RECURSO, CODIGO_RECURSO, ID_SALA) VALUES ('Monitor Phillips ZX45', 'MONITOR', 1);
insert into RECURSO(NOMBRE_RECURSO, CODIGO_RECURSO, ID_SALA) VALUES ('Proyector Samsung NG-850', 'PROYECTOR', 1);
insert into RECURSO(NOMBRE_RECURSO, CODIGO_RECURSO, ID_SALA) VALUES ('Pizarra Pix', 'PIZARRA', 1);


insert into RECURSO(NOMBRE_RECURSO, CODIGO_RECURSO, ID_SALA) VALUES ('Pizarra Pix-2', 'PIZARRA', 2);


insert into RECURSO(NOMBRE_RECURSO, CODIGO_RECURSO, ID_SALA) VALUES ('Monitor Phillips BB67', 'MONITOR', 3);


insert into RECURSO(NOMBRE_RECURSO, CODIGO_RECURSO, ID_SALA) VALUES ('Monitor Phillips ZX45', 'MONITOR', 4);
insert into RECURSO(NOMBRE_RECURSO, CODIGO_RECURSO, ID_SALA) VALUES ('Monitor Phillips BB67', 'MONITOR', 4);


insert into RECURSO(NOMBRE_RECURSO, CODIGO_RECURSO, ID_SALA) VALUES ('Monitor Phillips ZX45', 'MONITOR', 5);
insert into RECURSO(NOMBRE_RECURSO, CODIGO_RECURSO, ID_SALA) VALUES ('Monitor Phillips BB67', 'MONITOR', 5);


insert into RECURSO(NOMBRE_RECURSO, CODIGO_RECURSO, ID_SALA) VALUES ('Pizarra Pix-2', 'PIZARRA', 6);
insert into RECURSO(NOMBRE_RECURSO, CODIGO_RECURSO, ID_SALA) VALUES ('Proyector Samsung NG-1430', 'PROYECTOR', 6);
insert into RECURSO(NOMBRE_RECURSO, CODIGO_RECURSO, ID_SALA) VALUES ('Monitor Phillips BB67', 'MONITOR', 6);



-- Reservas
insert into RESERVA(ID_SALA, FECHA_INICIO, FECHA_FIN)
VALUES(1, parsedatetime('03/03/2020 10:00:00', 'dd/MM/yyyy hh:mm:ss'), PARSEDATETIME('03/03/2020 11:00:00', 'dd/MM/yyyy hh:mm:ss'));

insert into RESERVA(ID_SALA, FECHA_INICIO, FECHA_FIN)
VALUES(2, parsedatetime('03/03/2020 10:15:00', 'dd/MM/yyyy hh:mm:ss'), PARSEDATETIME('03/03/2020 11:30:00', 'dd/MM/yyyy hh:mm:ss'));

insert into RESERVA(ID_SALA, FECHA_INICIO, FECHA_FIN)
VALUES(3, parsedatetime('03/03/2020 09:00:00', 'dd/MM/yyyy hh:mm:ss'), PARSEDATETIME('03/03/2020 10:15:00', 'dd/MM/yyyy hh:mm:ss'));

insert into RESERVA(ID_SALA, FECHA_INICIO, FECHA_FIN)
VALUES(4, parsedatetime('03/03/2020 08:00:00', 'dd/MM/yyyy hh:mm:ss'), PARSEDATETIME('03/03/2020 11:50:00', 'dd/MM/yyyy hh:mm:ss'));

-- Las salas 5 y 6 parten sin reservas.
-- En el tramo de 10:00 a 10:14 solo se admiten las salas 2, 5 y 6.
