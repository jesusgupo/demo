Buenas.

Aquí tenéis el código hasta donde he llegado.

Incluyo dos apartados:

=================================

Arranque: 

	- Ejecutar la clase DemoApplication para que se inicialice el proyecto con Spring Boot.
	- Incluyo un XML para carga desde SoapUi con ejemplos de peticiones.

=================================

Consideraciones funcionales y técnicas:

	- Como veréis, las clases están con bastantes comentarios de las acciones que se desarrolla en cada metodo y comentarios	
	de cada anotacion incluida, indicando si es o no necesario. En caso de no serlo suelo incluirlo adicionalmente para
	que se tenga constancia de lo que la "magia" de Spring realiza por detrás. En otros casos me gusta indicarlas a modo
	de recordatorio de la configuración por defecto.
	
	- Los comentarios aplican igualmente a los POMs donde debe indicarse siempre el motivo de inclusion o exclusion de una dependencia.

	- En el application.properties veréis que he optado por la creación del esquema basándome en fichero y no por inicialización
	de JPA+Hibernate con el DDL generado. Tengo preferencia por incluir los scripts externos que generalmente vienen vinculados
	a las entregas de producto, de forma que en arranque se pueden detectar irregularidades de los scripts. En caso contrario,
	activat el DDL de Hibernate y eliminar la lectura de fichero.
	
	- Dejo activada la consola H2 para acceso vía Web.
	
	- En los repositorios de Spring he usado alternativamente JPQL y SQL Nativo. 
	
	- No he configurado ningún TransactionManager de manera expresa. Ni para el profile Dev ni para el de PRO. 
	En el de Dev se puede emplear el DataSourceTransactionManager que crea Spring por defecto para jdbc+H2 aunque siempre
	haciend join a las Tx abiertas en capa de negocio, lógicamente. No pierdo tiempo con esto.
	
	- He definido dos perfiles en Maven para desarrollo y generación de desplegable para PRO. En este caso he puesto que el 
	desplegable fuera un WAR para desplegar en un Java EE Server y la inicialización del datasource se realiza vía JNDI. 
	Todo es modificable. En este caso me faltaría añadir el Initializer de Spring Boot para el servidor en concreto. 
 
	- La definicion de operaciones CRUD. Añado dos comentarios:
		a) No he usado Spring Roo, cosa que me podría haber hecho ganar tiempo, pero he preferido que veáis el código detallado
		con algunas justificaciones.
		b) Los métodos CRUD están diseñados con los métodos HTTP recomendados GET, POST, PUT y DELETE aunque se podría valorar
		el empleo de put/post para las creaciones/modificaciones. Sería cuestión de analizarlo.
 
	- Se incluye la dependencia Maven de Lombok pero luego no he incluido ninguna anotacion propia para generacion de bytecode 	automático.
	
	- Como se puede ver, no he incluido ninguna configuración de trazas de log. Pendiente.
 
	- Se incluye un transformador de entidades de persistencia a entidades de dominio. En este caso he usado Orika. Hay muchos más. No 	tengo predilección por éste en concreto.
   
	- No he tenido en cuenta aspectos de negocio que pueden ser discutibles por aligerar el desarrollo: 
		a) Cuando elimino una sala elimino los recursos que posee. Lo normal sería desaginar los recursos, establecer su 
		clave ajena a nula y mandar los recursos al trastero con FK null. 
		
		Sin embargo, al no existir gestion de recursos
		lo dejo así por agilizar. Otra opción sería impedir eliminar una sala si ésta contuviera recursos.
		
		b) En la solicitud de la reserva, para conocer qué salas cumplen el aforo y la disponibilidad de recursos se
		ha trabajado con Entities. 
		
		Podría haber simplificado el código eliminando los filtros de Predicate aplicando la siguiente operativa: 
		
		Al obtener el listado de salas libres para la fecha/horas indicadas realizar la transformación a SalaTO 
		(basicamente, trabajar con entidades de dominio y no con entities de persistencia) incluyendo el listado de RecursosTO que 		dependen de cada sala. Si el RecursoTO implementa	Comparable en base al campo codigoRecurso (que es lo que se solicita via 
		interface REST) sería muy fácil conocer qué salas tienen los recursos puesto que se jugaría con la API de java.util.List 
		y el método containsAll que compararía los objetos por el código. Así evitaríamos el filtro-Predicate de Java 8. 
	
		c) No he hecho la optimización de huecos. 
		
		d) Lo ideal seria devolver, de entre todas las salas candidatas finales, aquella con menor número de aforo y que se ajuste
		más a la solicitud de reserva. En la operacion de reserva finalmente estoy devolviendo el primer elemento del listado filtrado.
		
		e) En la eliminación de salas, ésta no puede disponer de reservas asociadas (no es el caso de los recursos, por ejemplo, que
		estando también vinculadas en la misma cardinalidad que las reservas - 1:N entre salas/reservas y entre salas/recursos - pero
		los recursos propagan la eliminacion de la sala).
		
		
	- Se puede crear objeto retorno mejorado con un listado de errores de validacion. Debería orientar los servicios a
	intercambios de mensajes en lugar de a intercambio de resultados.
	
	- No he hecho una gestión de excepciones, ni he validado la existencia de salas con el mismo código o nombre cuando 
	se desea dar un alta, etc. Aspectos funcionales y demás. De hecho, en la gestión de salas en caso de no finalizar
	la operación retrno ACCEPTED. Se podría devolver algo más intuitivo como en el caso de la reserva.
	
	- No he añadido ningún tipo de validación de la colección JSR-303 Bean Validation como por ejemplo: En la reserva
	indicar que el minimo de asistentes sea uno, o fechas requeridas, etc. No he querido perder tiempo en esto.
   
	- De hecho, he mantenido casi las mismas entidades de negocio TO para la gestion, consulta, etc. Esto implica que los
	mismos atributos que se muestran en la consulta pueden ser editados. No he incluido control alguno sobre ello.
   
	- He añadido mínima documentación Swagger en el controlador y alguna operación, sin entrar en detalles ni definir el Model 
	para agilizar el desarrollo. La URL	de acceso queda en http://localhost:8080/demo/swagger-ui.html.
	
   
	- Funcionalmente, al principio diseñé más tablas con tipología de recursos, etc. Conforme he visto que no iba a tener
	tiempo he reducido el número de las mismas. El código único UQ de los recursos queda así eliminado y puede ser duplicado
	para facilitar la localización. Originalemente no era posible tener un recurso con idéntico código.
	
	- Se ha incluido lo más básico de seguridad con Spring Security: Habilitadas las peticiones POST vía REST, autenticación básica
	con usuario y password sdos y in-memory. Los proyectos Soapui ya incluyen el token.
	
	
   